# EasierLoggingAction

Add log action with params to be used by Yii log. This allow to log all action.

The category are set to `plugin.EasierLoggingAction.(post|param).(The controller).(The action).(The subaction for admin action)`.

The system log this information

- admin user (id and name)
- survey
- token
- complete uri
- ip address
- All post action if exist

each include inide bracket `[]`


Result create a one line log with each information inside `[]` 
```
[user:{$userid}-{$username}][sid:{$surveyid}][token:{$token}][uri:{$uri}][ip:{$ipaddress}]`
```

For post action: adding all `$_POST` encoded in json 
```
[user:{$userid}-{$username}][sid:{$surveyid}][token:{$token}][uri:{$uri}][ip:{$ipaddress}][ip:{$ipaddress}][json_encode($_POST)]`
```

You can choose to don't encode `$_POST`. Default is to encode to allow one line log, more easylly open in spreadsheet or other log analysis too.

## Sample in config.php

````
		'log' => array(
			'routes' => array(
				'getAction' => array(
					'class' => 'CFileLogRoute',
					'logFile' => 'actionGet.log',
					'categories'=>'plugin.EasierLoggingAction.param.*',
				),
				'postAction' => array(
					'class' => 'CFileLogRoute',
					'logFile' => 'actionPost.log',
					'categories'=>'plugin.EasierLoggingAction.post.*',
				),
            ),
        ),
````

See [Logging settings](https://manual.limesurvey.org/Optional_settings#Logging_settings) in LimeSurvey manual.

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright ©2021-2023 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Professional support](http://support.sondages.pro/)
- [Issues and contibute](https://gitlab.com/SondagesPro/coreAndTools/EasierLoggingAction)
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
