<?php
/**
 * EasierLoggingAction Plugin for LimeSurvey
 *
 * @author Denis Chenu <https://sondages.pro>
 * @copyright 2021-2023 Denis Chenu <https://sondages.pro>
 * @license AGPL v3
 * @version 1.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class EasierLoggingAction extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Add log action with params to be used by Yii log.';
    protected static $name = 'EasierLoggingAction';

    protected $settings = array(
        'jsonencode' => array(
            'type' => 'boolean',
            'label' => 'Json encode POST parameters.',
            'default' => 1
        ),
    );
    /**
     * Register to needed event
     */
    public function init()
    {
        $this->subscribe('beforeControllerAction', 'logAsInfo');
    }

    /**
     * Send a info log for easy login
     */
    public function logAsInfo()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            \Yii::log("Console action" , \CLogger::LEVEL_INFO, "plugin.EasierLoggingAction.console");
        }
        $controller = $this->getEvent()->get('controller');
        if(empty($controller)) {
            $subaction = "controller";
        }
        $action = $this->getEvent()->get('action');
        if(empty($action)) {
            $action = "action";
        }
        $subaction = $this->getEvent()->get('subaction');
        if(empty($subaction)) {
            $subaction = "subaction";
        }
        $userid = 0;
        $username = null;
        if(App()->getUser()) {
            $userid = App()->getUser()->getId();
            $username = App()->getUser()->getName();
        }
        $surveyid = App()->getRequest()->getParam('surveyid',App()->getRequest()->getParam('sid'));
        $token = App()->getRequest()->getParam('token');
        $uri = App()->getRequest()->getRequestUri();
        Yii::import('application.helpers.common_helper', true);
        $ipaddress = getIPAddress();
        $message = "[user:{$userid}-{$username}][sid:{$surveyid}][token:{$token}][uri:{$uri}][ip:{$ipaddress}]";

        if(App()->getRequest()->getIsPostRequest()) {
            $postVars = $_POST;
            if($this->get('jsonencode', null, null, 1)) {
                $postVars = json_encode($postVars);
            }
            $postVars = \CVarDumper::dumpAsString($postVars);
            \Yii::log($message . "[" . $postVars . "]", \CLogger::LEVEL_INFO, "plugin.EasierLoggingAction.post.{$controller}.{$action}.{$subaction}");
        } else {
            \Yii::log($message , \CLogger::LEVEL_INFO, "plugin.EasierLoggingAction.param.{$controller}.{$action}.{$subaction}");
        }
    }
}
